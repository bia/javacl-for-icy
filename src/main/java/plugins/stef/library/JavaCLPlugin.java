package plugins.stef.library;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * JavaCL library for Icy
 * 
 * @author Stephane Dallongeville
 */
public class JavaCLPlugin extends Plugin implements PluginLibrary
{
    //
}
